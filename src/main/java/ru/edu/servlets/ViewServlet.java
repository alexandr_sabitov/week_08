package ru.edu.servlets;

import ru.edu.db.CRUD;
import ru.edu.db.Record;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class ViewServlet extends HttpServlet {

    private final CRUD crud = CRUD.getInstance();

    @Override
    protected void doGet(final HttpServletRequest req,
                         final HttpServletResponse resp)
            throws ServletException, IOException {
        String id = req.getParameter("id");
        if (id == null) {
            resp.sendRedirect("/index?error=id_param_missing");
            return;
        }
        Record record = crud.getById(id);
        if (record == null) {
            resp.sendRedirect("/index?error=id_not_found");
            return;
        }

        req.setAttribute("records", record);

        req.getRequestDispatcher("/WEB-INF/view.jsp").forward(req, resp);
    }
}
