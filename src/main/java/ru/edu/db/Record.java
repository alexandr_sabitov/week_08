package ru.edu.db;
import java.math.BigDecimal;

public class Record {

    /**
     * Идентификатор объявлени.
     */
    private Long id;
    /**
     * Заголовок объявления.
     */
    private String title;
    /**
     * Тип Услуга/Покупка/Продажа.
     */
    private String type;
    /**
     * Цена объявления.
     */
    private BigDecimal price;
    /**
     * Текст объявления.
     */

    private String text;
    /**
     * Имя опубликовавшего.
     */
    private String publisher;
    /**
     * email опубликовавшего.
     */
    private String email;
    /**
     * Телефон опубликовавшего.
     */
    private String phone;
    /**
     * Ссылка на изображение.
     */
    private String pictureUrl;
    /**
     * getId().
     */
    public Long getId() {
        return id;
    }

    /**
     * setId.
     * @param id идентификатор.
     */
    public void setId(final Long id) {
        this.id = id;
    }
    /**
     * getTitle.
     */
    public String getTitle() {
        return title;
    }
    /**
     * setTitle.
     */
    public void setTitle(final String title) {
        this.title = title;
    }
    /**
     * getType.
     */
    public String getType() {
        return type;
    }
    /**
     * setType.
     */
    public void setType(final String type) {
        this.type = type;
    }
    /**
     * getPrice.
     */
    public BigDecimal getPrice() {
        return price;
    }
    /**
     * setPrice.
     */
    public void setPrice(BigDecimal price) {
        this.price = price;
    }
    /**
     * getText.
     */
    public String getText() {
        return text;
    }
    /**
     * setText.
     */
    public void setText(String text) {
        this.text = text;
    }
    /**
     * Возврат имени опубликовавшего.
     */
    public String getPublisher() {
        return publisher;
    }
    /**
     * Передача имени опубликовавшего.
     */
    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }
    /**
     * Возврат email.
     */
    public String getEmail() {
        return email;
    }
    /**
     * Передача email.
     */
    public void setEmail(String email) {
        this.email = email;
    }
    /**
     * phone
     */
    public String getPhone() {
        return phone;
    }

    /**
     *
     * @param phone номер телефона.
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }
    /**
     * Возврат url картинки
     */
    public String getPictureUrl() {
        return pictureUrl;
    }

    /**
     *
     * @param pictureUrl url картинки.
     */
    public void setPictureUrl(String pictureUrl) {
        this.pictureUrl = pictureUrl;
    }

    /**
     * Метод возвращающий начало текста объявления.
     * @param length
     * @return
     */
    public String getShort(int length){
        if(text.length() <= length){
            return text;
        }
        return text.substring(0, length)+"...";
    }

}