package ru.edu.db;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * CREATE, READ, UPDATE and DELETE.
 */
public class CRUD {

    /**
     * SELECT_ALL.
     */
    public static final String SELECT_ALL_SQL = "SELECT * FROM board";

    /**
     * SELECT_BY_ID.
     */
    public static final String SELECT_BY_ID = "SELECT * FROM "
            + "board WHERE id=?";

    /**
     * INSERT.
     */
    public static final String INSERT_SQL = "INSERT INTO board ("
            + "title, type, text, price, publisher, email, phone, picture_url)"
            + " VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";

    /**
     * UPDATE.
     */
    public static final String UPDATE_SQL = "UPDATE board "
            + "SET title=?, type=?, text=?, price=?, publisher=?, email=?, "
            + "phone=?, picture_url=? WHERE id=?";

    /**
     * Сеттер для облегчения тестирования.
     *
     * @param crud instance
     */
    public static void setInstance(final CRUD crud) {
        instance = crud;
    }

    /**
     * Статический экземпляр класса.
     */
    private static CRUD instance;

    /**
     * Переопределение.
     */
    private static CRUD override;

    /**
     * Источник данных.
     */
    private DataSource dataSource;

    /**
     * Конструктор с параметром.
     *
     * @param dataSourceTmp dataSource
     */
    public CRUD(final DataSource dataSourceTmp) {
        this.dataSource = dataSourceTmp;
    }

    /**
     * Синглтон.
     *
     * @return CRUD
     */
    public static CRUD getInstance() {
        synchronized (CRUD.class) {
            if (override != null) {
                return override;
            }
            if (instance == null) {
                try {
                    Context ctx = new InitialContext();
                    Context env = (Context) ctx.lookup("java:/comp/env");
                    DataSource dataSource = (DataSource) env.lookup(
                            "jdbc/dbLink");
                    instance = new CRUD(dataSource);
                } catch (Exception e) {
                    throw new RuntimeException(e.getMessage(), e);
                }
            }
        }
        return instance;
    }

    /**
     * Переопределение.
     *
     * @param overrideTmp override
     */
    public static void setOverride(final CRUD overrideTmp) {
        CRUD.override = overrideTmp;
    }

    /**
     * Подключение к БД.
     *
     * @return dataSource
     */
    private Connection getConnection() {
        try {
            return dataSource.getConnection();
        } catch (SQLException e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    /**
     * Получение всех записей из БД.
     *
     * @return query
     */
    public List<Record> getIndex() {
        return query(SELECT_ALL_SQL);
    }

    /**
     * Получение записи по id из БД.
     *
     * @param id id
     * @return запрос
     */
    public Record getById(final String id) {
        List<Record> record = query(SELECT_BY_ID, id);
        if (record.isEmpty()) {
            return null;
        }
        return record.get(0);
    }

    /**
     * Сохранение записи в БД.
     *
     * @param record record
     * @return id
     */
    public Long save(final Record record) {
        Long id;
        if (record.getId() == null) {
            id = insert(record);
        } else {
            id = update(record);
        }
        return id;
    }

    /**
     * Вставка записи в БД.
     *
     * @param record record
     * @return id
     */
    public Long insert(final Record record) {
        Long id = execute(INSERT_SQL, record.getTitle(), record.getType(),
                record.getText(), (record.getPrice() == null)
                        ? new BigDecimal(0) : record.getPrice()
                        .toPlainString(),
                record.getPublisher(), record.getEmail(),
                record.getPhone(), record.getPictureUrl());
        return id;
    }

    /**
     * Обновление записи в БД.
     *
     * @param record record
     * @return id
     */
    public Long update(final Record record) {
        Long id = execute(UPDATE_SQL, record.getTitle(), record.getType(),
                record.getText(), (record.getPrice() == null)
                        ? new BigDecimal(0) : record.getPrice()
                        .toPlainString(),
                record.getPublisher(), record.getEmail(),
                record.getPhone(), record.getPictureUrl(),
                record.getId());
        return id;
    }

    private Long execute(final String sql, final Object... values) {
        try (PreparedStatement statement = getConnection()
                .prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)) {
            for (int i = 0; i < values.length; i++) {
                statement.setObject(i + 1, values[i]);
            }
            Long id = null;
            statement.executeUpdate();
            ResultSet resSet = statement.getGeneratedKeys();
            if (resSet.next()) {
                id = resSet.getLong(1);
            }
            return id;
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    private List<Record> query(final String sql, final Object... values) {
        try (PreparedStatement statement = getConnection()
                .prepareStatement(sql)) {
            for (int i = 0; i < values.length; i++) {
                statement.setObject(i + 1, values[i]);
            }
            ResultSet resultSet = statement.executeQuery();

            List<Record> result = new ArrayList<>();
            while (resultSet.next()) {
                result.add(map(resultSet));
            }
            return result;
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    private Record map(final ResultSet resultSet) throws SQLException {
        Record record = new Record();

        record.setId(resultSet.getLong("id"));
        record.setTitle(resultSet.getString("title"));
        record.setType(resultSet.getString("type"));
        record.setPrice((resultSet.getString("price") == null)
                ? new BigDecimal(0) : new BigDecimal(resultSet
                .getString("price")));
        record.setText(resultSet.getString("text"));
        record.setPublisher(resultSet.getString("publisher"));
        record.setEmail(resultSet.getString("email"));
        record.setPhone(resultSet.getString("phone"));
        record.setPictureUrl(resultSet.getString("picture_url"));

        return record;
    }
}
