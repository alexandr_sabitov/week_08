package ru.edu.db;


import org.junit.BeforeClass;
import org.junit.Test;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class CRUDTest {
    private static DataSource dataSourceMock = mock(DataSource.class);
    private static Connection connectionMock = mock(Connection.class);

    private static PreparedStatement indexStatement = mock(PreparedStatement.class);
    private static PreparedStatement byIdStatement = mock(PreparedStatement.class);
    private static PreparedStatement insertStatement = mock(PreparedStatement.class);
    private static PreparedStatement updateStatement = mock(PreparedStatement.class);

    private static ResultSet indexResult = mock(ResultSet.class);
    private static ResultSet byIdResult = mock(ResultSet.class);
    private static ResultSet insertResult = mock(ResultSet.class);
    private static ResultSet updateResult = mock(ResultSet.class);

    @BeforeClass
    public static void setupJndi() throws NamingException, SQLException {
        System.setProperty(Context.INITIAL_CONTEXT_FACTORY,
                "org.osjava.sj.SimpleContextFactory");
        System.setProperty("org.osjava.sj.jndi.shared", "true");

        Context ic = new InitialContext();
        ic.bind("java:/comp/env", ic);
        ic.bind("java:/comp/env/jdbc/dbLink", dataSourceMock);
        ic.bind("jdbc/dbLink", dataSourceMock);
        Context ctx = new InitialContext();
        Context env  = (Context)ctx.lookup("java:/comp/env");
        DataSource dataSource = (DataSource) env.lookup("jdbc/dbLink");
        assertEquals(dataSourceMock, dataSource);
        when(dataSource.getConnection()).thenReturn(connectionMock);

        when(connectionMock.prepareStatement(CRUD.SELECT_ALL_SQL)).thenReturn(indexStatement);
        when(indexStatement.executeQuery()).thenReturn(indexResult);
        when(indexResult.next()).thenReturn(true).thenReturn(true).thenReturn(false);
        when(indexResult.getLong("id")).thenReturn(1L).thenReturn(2L);
        when(indexResult.getString("title")).thenReturn("title").thenReturn("anotherTitle");
        when(indexResult.getString("type")).thenReturn("type").thenReturn("anotherType");
        when(indexResult.getString("price")).thenReturn("1231").thenReturn("1232");
        when(indexResult.getString("text")).thenReturn("text").thenReturn("anotherText");
        when(indexResult.getString("publisher")).thenReturn("publisher").thenReturn("anotherPublisher");
        when(indexResult.getString("email")).thenReturn("email").thenReturn("anotherEmail");
        when(indexResult.getString("phone")).thenReturn("phone").thenReturn("anotherPhone");
        when(indexResult.getString("picture_url")).thenReturn("picture_url").thenReturn("anotherUrl");

        when(connectionMock.prepareStatement(CRUD.SELECT_BY_ID)).thenReturn(byIdStatement);
        when(byIdStatement.executeQuery()).thenReturn(byIdResult);
        when(byIdResult.next()).thenReturn(true).thenReturn(false);
        when(byIdResult.getLong("id")).thenReturn(1L);
        when(byIdResult.getString("title")).thenReturn("title");
        when(byIdResult.getString("type")).thenReturn("type");
        when(byIdResult.getString("price")).thenReturn("1231");
        when(byIdResult.getString("text")).thenReturn("text");
        when(byIdResult.getString("publisher")).thenReturn("publisher");
        when(byIdResult.getString("email")).thenReturn("email");
        when(byIdResult.getString("phone")).thenReturn("phone");
        when(byIdResult.getString("picture_url")).thenReturn("picture_url");

        when(connectionMock.prepareStatement(CRUD.INSERT_SQL, Statement.RETURN_GENERATED_KEYS)).thenReturn(insertStatement);
        when(insertStatement.executeQuery()).thenReturn(insertResult);
        when(insertResult.next()).thenReturn(true).thenReturn(false);
        when(insertStatement.getGeneratedKeys()).thenReturn(insertResult);
        when(insertResult.getLong(1)).thenReturn(100L);

        when(connectionMock.prepareStatement(CRUD.UPDATE_SQL, Statement.RETURN_GENERATED_KEYS)).thenReturn(updateStatement);
        when(updateStatement.executeQuery()).thenReturn(updateResult);
        when(updateResult.next()).thenReturn(true).thenReturn(false);
        when(updateStatement.getGeneratedKeys()).thenReturn(updateResult);
        when(updateResult.getLong(1)).thenReturn(100L);
    }

    @Test
    public void getInstance() {
        CRUD instance = CRUD.getInstance();
        assertNotNull(instance);
    }

    @Test
    public void getIndex() {
        CRUD instance = CRUD.getInstance();
        List<Record> records = instance.getIndex();
        assertEquals(2, records.size());
        assertEquals((Long)1L, records.get(0).getId());
        assertEquals("title", records.get(0).getTitle());
        assertEquals((Long)2L, records.get(1).getId());
        assertEquals("anotherTitle", records.get(1).getTitle());
    }

    @Test
    public void getById() throws SQLException {
        CRUD instance = CRUD.getInstance();
        Record r = instance.getById("1");
        assertEquals((Long)1L, r.getId());
        assertEquals("title", r.getTitle());

        Record r2 = instance.getById("2");
        assertNull(r2);
    }

    @Test
    public void save() {
        Record insert = new Record();
        CRUD instance = CRUD.getInstance();
        Long id = instance.save(insert);
        assertEquals(Long.valueOf(100), id);

        Record update = new Record();
        update.setId(100L);
        Long uid = instance.save(update);
        assertEquals(Long.valueOf(100), uid);
    }
}