package ru.edu.servlets;

import org.junit.Test;
import ru.edu.db.CRUD;
import ru.edu.db.Record;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class IndexServletTest {
    @Test
    public void doGet() throws ServletException, IOException {
        CRUD crudMock = mock(CRUD.class);
        CRUD.setInstance(crudMock);

        List<Record> emptyList = Collections.emptyList();
        when(crudMock.getIndex()).thenReturn(emptyList);

        IndexServlet servlet = new IndexServlet();
        HttpServletRequest req = mock(HttpServletRequest.class);
        HttpServletResponse resp = mock(HttpServletResponse.class);
        RequestDispatcher requestDispatcher = mock(RequestDispatcher.class);
        when(req.getRequestDispatcher("/WEB-INF/index.jsp")).thenReturn(requestDispatcher);

        servlet.doGet(req, resp);
        verify(req, times(1)).setAttribute("records", emptyList);;
        verify(requestDispatcher, times(1)).forward(req, resp);
    }
}