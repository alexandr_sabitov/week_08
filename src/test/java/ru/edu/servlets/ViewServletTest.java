package ru.edu.servlets;
import org.junit.Test;
import ru.edu.db.CRUD;
import ru.edu.db.Record;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

import static org.mockito.Mockito.any;
import static org.mockito.Mockito.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class ViewServletTest {
    @Test
    public void doGet() throws SQLException, ServletException, IOException {
        CRUD crudMock = mock(CRUD.class);
        CRUD.setInstance(crudMock);

        Record r = new Record();
        r.setId(123L);
        when(crudMock.getById("123")).thenReturn(r);

        ViewServlet servlet = new ViewServlet();
        HttpServletRequest req = mock(HttpServletRequest.class);
        HttpServletResponse resp = mock(HttpServletResponse.class);
        when(req.getParameter("id")).thenReturn("123");

        RequestDispatcher requestDispatcher = mock(RequestDispatcher.class);
        when(req.getRequestDispatcher("/WEB-INF/view.jsp")).thenReturn(requestDispatcher);

        servlet.doGet(req, resp);
        verify(crudMock, times(1)).getById(any(String.class));
        verify(req, times(1)).setAttribute(eq("records"), any(Record.class));
        verify(requestDispatcher, times(1)).forward(req, resp);
    }
}