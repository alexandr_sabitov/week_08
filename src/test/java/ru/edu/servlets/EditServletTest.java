package ru.edu.servlets;

import org.junit.Test;
import ru.edu.db.CRUD;
import ru.edu.db.Record;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class EditServletTest {
    @Test
    public void doGet() throws ServletException, IOException, SQLException {
        CRUD crudMock = mock(CRUD.class);
        CRUD.setInstance(crudMock);

        Record r = new Record();
        r.setId(123L);
        when(crudMock.getById("123")).thenReturn(r);

        EditServlet servlet = new EditServlet();
        HttpServletRequest req = mock(HttpServletRequest.class);
        HttpServletResponse resp = mock(HttpServletResponse.class);
        when(req.getParameter("id")).thenReturn("123");

        RequestDispatcher requestDispatcher = mock(RequestDispatcher.class);
        when(req.getRequestDispatcher("/WEB-INF/edit.jsp")).thenReturn(requestDispatcher);

        servlet.doGet(req, resp);
        verify(req, times(1)).setAttribute("records", r);
        verify(requestDispatcher, times(1)).forward(req, resp);
        verify(crudMock, times(1)).getById(any(String.class));
    }

    @Test
    public void doPost() throws ServletException, IOException {
        CRUD crudMock = mock(CRUD.class);
        CRUD.setInstance(crudMock);
        List<Record> emptyList = Collections.emptyList();
        when(crudMock.getIndex()).thenReturn(emptyList);

        EditServlet servlet = new EditServlet();
        HttpServletRequest req = mock(HttpServletRequest.class);
        HttpServletResponse resp = mock(HttpServletResponse.class);

        Map paramMap = spy(new HashMap());
        paramMap.put("id", param("123"));
        paramMap.put("email", param("email"));
        paramMap.put("phone", param("phone"));
        paramMap.put("price", param("1111"));
        paramMap.put("publisher", param("publisher"));
        paramMap.put("text", param("text"));
        paramMap.put("type", param("type"));
        paramMap.put("title", param("title"));
        paramMap.put("picture_url", param("https://test.test"));
        when(req.getParameterMap()).thenReturn(paramMap);

        doAnswer((invocationOnMock) -> {
            Record record = invocationOnMock.getArgument(0);

            assertEquals((Long)123L, record.getId());
            assertEquals(new BigDecimal(1111), record.getPrice());
            assertEquals("text", record.getText());
            return null;
        }).when(crudMock).save(any(Record.class));

        servlet.doPost(req, resp);
        verify(paramMap, times(1)).get("id");
        verify(crudMock, times(1)).save(any(Record.class));
    }

    private String[] param(String value) {
        return new String[]{value};
    }
}